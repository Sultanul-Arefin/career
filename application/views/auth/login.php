   
<div class="container">
    <div class="card sign_up_card">
      <article class="card-body col-sm-6 m-auto">
          <h4 class="card-title mt-3 text-center">Log In</h4>
          <form method="post" action="<?php echo base_url('auth/logged'); ?>">
              <div class="form-group input-group group_mg_btm">
                  <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                  </div>
                  <input name="email" class="form-control fch" placeholder="Email address" type="email">
              </div> <!-- form-group// -->

              <div class="form-group input-group group_mg_btm">
                <div class="input-group-prepend">
                  <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                </div>
                <input class="form-control fch" placeholder="Enter password" type="password" name="password">
              </div> <!-- form-group// -->

              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Log In</button>
              </div> <!-- form-group// -->    

            <p class="text-center">Don't have an account? <a href="<?= base_url('auth/signup');?>">Sign Up</a> </p>               
        </form>
      </article>
    </div>
</div> 
