   <style type="text/css">
       table td{
        word-wrap: break-word;

       }
   </style>
  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">
      
        <div class="row">
            <div class="col-lg-12">
                <table id="example" class="display nowrap" style="table-layout: fixed; width: 100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Post Title</th>
                            <th>Post Heading</th>
                            <th>Action</th>
                            
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($key as $detail): ?>
                        <tr>

                           <td><?php echo $detail->id; ?></td>
                           <td><?php echo $detail->cat_name; ?></td>
                           <td><?php echo $detail->sub_cat_name; ?></td>
                           <td><?php echo $detail->post_title; ?></td>
                           <td><?php echo $detail->post_heading; ?></td>
                          
                           
                            <td><a href="<?php echo base_url('admin/approve/').$detail->id; ?>" class="btn btn-danger" onclick="return confirm('Do you want to approve this post?');" title="Approve"><i class="fa fa-check"></i></a><a href="<?php echo base_url('admin/pending_delete/').$detail->id; ?>" class="btn btn-danger" onclick="return confirm('Do you want to delete this post?');" title="Delete"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->