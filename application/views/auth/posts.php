<link rel="stylesheet" href="<?= base_url('assets/css/login_register.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/utilities.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">



<div class="container category">
	<div class="card sign_up_card">
		<article class="card-body col-sm-6 m-auto">
			<h4 class="card-title mt-3 text-center">Add Post</h4>
			<form method="post" action="<?= base_url('admin/save_post') ?>" enctype="multipart/form-data">
			<select name="cat_name" class="custom-select select form-group">
				<option selected> Select Category</option>
				<?php foreach($category as $data): ?>
				<option value="<?= $data->cat_name; ?>"><?= $data->cat_name; ?></option>
				<?php endforeach; ?>
			</select> 

			<select name="sub_cat_name" class="custom-select select form-group">
				<option selected> Select Sub Category</option>
				<?php foreach($sub_category as $data): ?>
				<option value="<?= $data->sub_cat_name; ?>"><?= $data->sub_cat_name; ?></option>
				<?php endforeach; ?>
			</select> 
  
  <!-- form-group// -->
<!-- form-group// -->

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input class="form-control fch" placeholder="post title" type="text" name="post_title">
				</div> <!-- form-group// -->
				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input class="form-control fch" placeholder="post heading" type="text" name="post_heading">
				</div> 

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input class="form-control fch" placeholder="post description" type="text" name="post_description">
				</div> 

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input type="file" id="myFile" name="video_url">
				</div>
				
                <div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input class="form-control fch" placeholder="requirments" type="text" name="requirments">
				</div>

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input class="form-control fch" placeholder="institution" type="text" name="institution">
				</div>

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input class="form-control fch" placeholder="demand" type="text" name="demand">
				</div>

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input class="form-control fch" placeholder="skills" type="text" name="skills">
				</div>

				<div class="form-group input-group group_mg_btm">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input class="form-control fch" placeholder="conclusion" type="text" name="conclusion">
				</div>
				<input placeholder="conclusion" type="hidden" name="status" value="active">
				
				
				
				
			
				<!-- form-group// -->
                
                <!-- form-group// -->

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block">Insert</button>
				</div> <!-- form-group// -->    

				                                                                 
			</form>
		</article>
	</div> <!-- card.// -->
</div> 

