
<div class="container container_explore">

    

    <?php if($category != null){ ?>
        <h2 class="primary_heading_explore">
            DISCOVER CAREER PATH's AND CHOOSE YOUR's
        </h2>
        <?php foreach($category as $data): ?>
    <div class="row explore_card relative_pos">
        <p class="category">
            Discover Career's Of <span><?php echo $data->cat_name; ?></span> Background
        </p>
        <div class="col-md-6 border-right">
            <div class="video_container">
                <video src="<?= base_url().$data->video_url; ?>" controls></video>
            </div>
        </div>
        <div class="col-md-6 vertical_middle">
            <div class="description_container">
                <h3 class="desc_heading">
                   <?php echo $data->cat_heading; ?>
                </h3>

                <p class="short_desc">
                    <?php echo $data->cat_description; ?>
                </p>

                <center>
                    <a href="<?= base_url() ?>welcome/category/<?php echo $data->cat_name; ?>" class="btn btn-info btn-lg" title="Explore All Career Path's From <?php echo $data->cat_name; ?> Background">View All</a>
                </center>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php }else{ ?>
    <h2 class="primary_heading_explore">
        NO DATA AVAILABLE
    </h2>
<?php } ?>
</div>