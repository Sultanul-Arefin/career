
<div class="container container_explore">
    
    <?php if($post!=null){ ?>
        <h2 class="primary_heading_explore">
            DISCOVER CAREER PATH's OF <span style="text-transform:uppercase"><?= $post[0]->sub_cat_name; ?></span>
        </h2>
        <?php foreach($post as $value): ?>
    <div class="row explore_card relative_pos">
        <p class="category">
            Discover Career Of <span><?= $value->post_title; ?></span>
        </p>
        <div class="col-md-6 border-right">
            <div class="video_container">
                <video src="<?= base_url().$value->video_url; ?>" controls></video>
            </div>
        </div>
        <div class="col-md-6 vertical_middle">
            <div class="description_container">
                <h3 class="desc_heading">
                    <?= $value->post_heading; ?>
                </h3>

                <p class="short_desc">
                <?= $value->post_description; ?>
                </p>

                <center>
                    <a href="<?=base_url('') ?>welcome/posts/<?= $value->id; ?>" class="btn btn-info btn-lg" title="Read Details about <?= $value->post_title; ?>">View Details</a>
                </center>
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php }else{ ?>
    <h2 class="primary_heading_explore">
        NO DATA AVAILABLE
    </h2>
<?php } ?>
</div>