
<div class="container container_explore">

<?php if($sub_category != null){ ?>
<h2 class="primary_heading_explore">
    DISCOVER CAREER PATH's OF <span style="text-transform:uppercase">
    <?= $sub_category[0]->cat_name; ?>
    </span>
</h2>
<?php }else { ?>

<h2 class="primary_heading_explore">
    No Data Avaiable
</h2>
<?php } ?>

<?php if($sub_category != null): ?>
    <?php foreach($sub_category as $data): ?>
<div class="row explore_card relative_pos">
    <p class="category">
        Discover Career's Of <span><?php echo $data->sub_cat_name; ?></span>
    </p>
    <div class="col-md-6 border-right">
        <div class="video_container">
            <video src="<?= base_url().$data->sub_cat_video_url; ?>" controls></video>
        </div>
    </div>
    <div class="col-md-6 vertical_middle">
        <div class="description_container">
            <h3 class="desc_heading">
                <?php echo $data->sub_cat_heading; ?>
            </h3>

            <p class="short_desc">
                <?php echo $data->sub_cat_description; ?>
            </p>

            <center>
                <a href="<?= base_url() ?>welcome/subCategory/<?php echo $data->sub_cat_name; ?>" class="btn btn-info btn-lg" title="Explore All Career Path's From <?php echo $data->sub_cat_name; ?> Background">View All</a>
            </center>
        </div>
    </div>
</div>
<?php endforeach; ?>
<?php endif; ?>



</div>