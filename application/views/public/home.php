
    <!-- header content starts -->
    <div class="header container-fluid home_background">
        <div class="header_content">
            <p class="welcome_msg">
                Hello!! Welcome To 
            </p>
            <h3 class="primary_heading">
                Explore Career
            </h3>

            <!-- header search -->
            <form class="">
                <div class="input-group mb-3">
                    <input type="text" class="form-control search_input" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-success search_btn" type="button">Search</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    <!-- header content ends -->

    <!-- Home Content Starts-->

     <div class="ticker-container">
            <div class="ticker-caption">
                <p>Job News</p>
            </div>
            <ul>
                <?php foreach($sticker as $stick): ?>
                <div>
                    <li><span>Job Portal <?php echo $stick->id; ?> &ndash; <a href="<?php echo $stick->url; ?>"><?php echo $stick->title; ?></a></span></li>
                </div>
            <?php endforeach; ?>
                
            </ul>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="<?= base_url('assets/js/ticker.js') ?>"></script>
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') +
                    '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
        </script>



    <div class="container home_content_container">
        <div class="content_container_inner">
            <h3 class="home_heading">
                ADVICE FOR MAKING A FRESH START
            </h3>

            <p class="home_desc">
                Life is full of new beginnings. Here’s some valuable advice to help you along the way.
            </p>

            <div class="qoutes">
                <h4 class="qoute_heading">
                    STEVE JOBS – STANFORD UNIVERSITY, 2005
                </h4>
                <p class="qoute_desc">
                    "You've got to find what you love. And that is as true for your work as it is for your lovers. Your work is going to fill a large part of your life, and the only way to be truly satisfied is to do what you believe is great work. And the only way to do great work is to love what you do."
                </p>
            </div>

            <div class="qoutes">
                <h4 class="qoute_heading">
                    STEPHEN COLBERT – NORTHWESTERN UNIVERSITY, 2011
                </h4>
                <p class="qoute_desc">
                
                    "If we'd all stuck with our first dream, the world would be overrun with cowboys and princesses. So whatever your dream is right now, if you don't achieve it, you haven't failed, and you're not some loser. But just as importantly — and this is the part I may not get right and you may not listen to — if you do get your dream, you are not a winner."
                </p>
            </div>


            <div class="qoutes">
                <h4 class="qoute_heading">
                    NORA EPHRON – WELLESLEY COLLEGE, 1996
                </h4>
                <p class="qoute_desc">
                    "What are you going to do? Everything, is my guess. It will be a little messy, but embrace the mess. It will be complicated, but rejoice in the complications. It will not be anything like what you think it will be like, but surprises are good for you. And don't be frightened: You can always change your mind. I know: I've had four careers and three husbands."
                </p>
            </div>
        </div>
    </div>
    <!-- Home Content ends-->

    <script>
        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            // Do something
            if(scroll > 0){
                $('.explore_nav').addClass('explore_nav_common');
            }else{
                $('.explore_nav').removeClass('explore_nav_common');
            }
        });
    </script>

    