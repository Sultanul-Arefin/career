<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Career Explore</title>
    <link rel="shortcut icon" type="image/png" href="<?= base_url('assets/images/logo/log.PNG'); ?>">

    <!-- Frame Works And fonts-->
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,700,800" rel="stylesheet">

    <!-- Custom stylings -->
    <link rel="stylesheet" href="<?= base_url('assets/css/navbar.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/home_header.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/footer.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/utilities.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/login_register.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/main.css') ?>">
    <script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
</head>
<body style="<?php if($this->uri->segment(1) != ''){
        echo 'padding-top:69px;';
    } ?>">
    
    <!-- navbar starts   add this ' explore_nav_common' class to other pages-->
    <nav class="navbar navbar-expand-lg explore_nav <?php if($this->uri->segment(1) != ''){
        echo 'explore_nav_common';
    } ?>">
        <a class="navbar-brand" href="<?= base_url(); ?>">
            <img src="<?= base_url('assets/images/logo/log.PNG'); ?>" alt="logo" class="logo_main">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url();?>">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('/welcome/explore');?>">Explore Career</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('welcome/popular_category'); ?>">Popular Category</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('welcome/about_us'); ?>">About</a>
                </li>

                <?php if($this->session->userdata('logged')): ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('welcome/add_user_post'); ?>">Add Post</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo $this->session->userdata('logged'); ?>
                    </a>
                    <div class="dropdown-menu custom_dropdown" aria-labelledby="navbarDropdown">
                        <?php if($this->session->userdata('u_level') == 'admin'): ?>
                       
                        <a class="dropdown-item" href="<?php echo base_url('admin/users'); ?>">Dashboard</a>
                        <?php endif; ?>
                        <a class="dropdown-item" href="<?php echo base_url('welcome/query'); ?>">Explore</a>
                        <a class="dropdown-item" href="<?php echo base_url('auth/logout'); ?>">Log Out</a>
                    </div>
                </li>
            <?php endif; ?>

                <?php if(!$this->session->userdata('logged')): ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('auth/login');?>">Sign In</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('auth/signup');?>">Sign Up</a>
                </li>
            <?php endif; ?>
            </ul>
        </div>
    </nav>
    <!-- navbar ends -->
