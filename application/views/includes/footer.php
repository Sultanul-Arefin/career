
    <div class="container-fluid footer_container">
        <div class="footer">
            <div class="row mglr_zero">
                <div class="col-sm-4 vertical_middle">
                    <center>
                        <a class="navbar-brand" href="<?= base_url(); ?>">
                            <img src="<?= base_url('assets/images/logo/log.PNG'); ?>" alt="logo" class="logo_main">
                        </a>
                    </center>
                </div>
                <div class="col-sm-4 vertical_middle">
                    <p class="copyright">&copy; Explore Career <?= date("Y"); ?> | All Rights Reserved</p>
                </div>
                <div class="col-sm-4 vertical_middle">
                    <ul class="social-links">
                        <li>
                            <a href="https://www.facebook.com" target="_blank" class="wow fadeInUp">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com" target="_blank" class="wow bounceInDown" data-wow-delay=".1s">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com" target="_blank" class="wow fadeInUp" data-wow-delay=".2s">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.pinterest.com" target="_blank" class="wow fadeInUp" data-wow-delay=".4s">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>

    
    <script src="<?= base_url('assets/js/popper.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/main.js') ?>"></script>

</html>