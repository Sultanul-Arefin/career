<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Career Explore</title>
    <link rel="shortcut icon" type="image/png" href="<?= base_url('assets/images/logo/log.PNG'); ?>">

    <!-- Frame Works And fonts-->
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,700,800" rel="stylesheet">

    <script src="<?= base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
  
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
</head>
<body>
    

    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/dashboard.css'); ?>">
     <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
    <i class="fa fa-bars"></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <a href="<?= base_url(); ?>">Home</a>
        <div id="close-sidebar">
          <i class="fa fa-times"></i>
        </div>
      </div>
      <div class="sidebar-header">
        <div class="user-pic">
          <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg"
            alt="User picture">
        </div>
        <div class="user-info">
          <span class="user-name">Admin
            <strong>Admin</strong>
          </span>
          <span class="user-role">Administrator</span>
          
        </div>
      </div>
      <!-- sidebar-header  -->
      
      <!-- sidebar-search  -->
      <div class="sidebar-menu">
        <ul>
          <li class="header-menu">
            <span>Admin</span>
          </li>
          <li class="sidebar-dropdown active">
            <a href="#">
              <i class="fa fa-tasks"></i>
              <span>Dashboard</span>
             
            </a>
            <div class="sidebar-submenu" style="display: block;">
              <ul>

                <li>
                   <a class="dropdown-item" href="<?php echo base_url('admin/users'); ?>">View Users</a>
                </li>

                <li>
                   <a class="dropdown-item" href="<?php echo base_url('admin/pending_post'); ?>">View Pending Post</a>
                </li>

                <li>
                   <a class="dropdown-item" href="<?php echo base_url('admin/category'); ?>">Add Category</a>
                </li>

                <li>
                   <a class="dropdown-item" href="<?php echo base_url('admin/sub_category'); ?>">Add Sub Category</a>
                </li>

                <li>
                   <a class="dropdown-item" href="<?php echo base_url('admin/add_news'); ?>">Add News</a>
                </li>

                <li>
                   <a class="dropdown-item" href="<?php echo base_url('admin/post'); ?>">Add Post</a>
                </li>
              </ul>
            </div>
          </li>
         
        </ul>
      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
    <div class="sidebar-footer">
     
    </div>
  </nav>

    <!-- navbar ends -->
