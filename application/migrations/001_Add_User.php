<?php

	class Migration_Add_user extends CI_Migration{

		public function up(){
			$this->load->dbforge();

			$this->dbforge->add_field(
				array(
					'id' => array(
						'type' => 'INT',
						'constant' => 5,
						'unsigned' => TRUE,
            			'auto_increment' => TRUE
					),
					'username'=>array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'email' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'password' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'user_level' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'user_image' => array(
						'type' => 'VARCHAR',
						'constraint' => '500'
					),
					'created_on' => array(
            			'type' =>  'TIMESTAMP'
        			)
				)
			);
			$this->dbforge->add_key('id', TRUE);
			$this->dbforge->create_table('users');

		}

		public function down(){
			$this->load->dbforge();
			$this->dbforge->drop_table('users');
		}
	}

?>