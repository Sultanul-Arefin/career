<?php

	class Migration_Add_Category extends CI_Migration{

		public function up(){
			$this->load->dbforge();

			$this->dbforge->add_field(
				array(
					'id' => array(
						'type' => 'INT',
						'constant' => 5,
						'unsigned' => TRUE,
            			'auto_increment' => TRUE
					),
					'cat_name'=>array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'cat_heading' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'cat_description' => array(
						'type' => 'VARCHAR',
						'constraint' => '1000'
					),
					'video_url' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'created_on' => array(
            			'type' =>  'TIMESTAMP'
        			)
				)
			);
			$this->dbforge->add_key('id', TRUE);
			$this->dbforge->create_table('category');

			$this->dbforge->add_field(
				array(
					'id' => array(
						'type' => 'INT',
						'constant' => 5,
						'unsigned' => TRUE,
            			'auto_increment' => TRUE
					),
					'sub_cat_name'=>array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'sub_cat_heading' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'sub_cat_description' => array(
						'type' => 'VARCHAR',
						'constraint' => '1000'
					),
					'sub_cat_video_url' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'cat_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'created_on' => array(
            			'type' =>  'TIMESTAMP'
        			)
				)
			);
			$this->dbforge->add_key('id', TRUE);
			$this->dbforge->create_table('sub_category');

			$this->dbforge->add_field(
				array(
					'id' => array(
						'type' => 'INT',
						'constant' => 5,
						'unsigned' => TRUE,
            			'auto_increment' => TRUE
					),
					'sub_cat_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'cat_name'=>array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'post_title' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'post_heading' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'post_description' => array(
						'type' => 'VARCHAR',
						'constraint' => '1000'
					),
					'video_url' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'requirments' => array(
						'type' => 'VARCHAR',
						'constraint' => '1000'
					),
					'institution' => array(
						'type' => 'VARCHAR',
						'constraint' => '1000'
					),
					'demand' => array(
						'type' => 'VARCHAR',
						'constraint' => '1000'
					),
					'skills' => array(
						'type' => 'VARCHAR',
						'constraint' => '1000'
					),
					'conclusion' => array(
						'type' => 'VARCHAR',
						'constraint' => '1000'
					),
					'created_on' => array(
            			'type' =>  'TIMESTAMP'
        			)
				)
			);
			$this->dbforge->add_key('id', TRUE);
			$this->dbforge->create_table('posts');

		}

		public function down(){
			$this->load->dbforge();
			$this->dbforge->drop_table('category');
			$this->dbforge->drop_table('sub_category');
			$this->dbforge->drop_table('posts');
		}
	}

?>