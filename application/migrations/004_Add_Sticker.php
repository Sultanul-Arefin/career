<?php

	class Migration_Add_Sticker extends CI_Migration{

		public function up(){
			$this->load->dbforge();

			$this->dbforge->add_field(
				array(
					'id' => array(
						'type' => 'INT',
						'constant' => 5,
						'unsigned' => TRUE,
            			'auto_increment' => TRUE
					),
					'title'=>array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					),
					'url' => array(
						'type' => 'VARCHAR',
						'constraint' => '100'
					)
				)
			);
			$this->dbforge->add_key('id', TRUE);
			$this->dbforge->create_table('sticker');

		}

		public function down(){
			$this->load->dbforge();
			$this->dbforge->drop_table('sticker');
		}
	}

?>