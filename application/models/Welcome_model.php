<?php

	class Welcome_model extends CI_Model{

		function __construct(){
			parent::__construct();
		}

		public function insert($data){
			return $this->db->insert('messages', $data);
		}

		public function fetch_sticker(){
			$this->db->select('*');
			$this->db->from('sticker');
			$data = $this->db->get();
			return $data->result();
		}

		function exam_result(){
			

			$this->db->select('*');
			$this->db->from('platform');
			$query = $this->db->get();

			return $query->result_array();
		}

		function fetch_comment($id){
			$this->db->select('*');
			$this->db->from('comments');
			 $this->db->where('c_blog_id',$id);
			 $result = $this->db->get();
			return $result->result();
		}

		function getUserName($uid){
			$this->db->select('*');
                $this->db->from('users');
                $this->db->where('id', $uid);

                $query = $this->db->get();

                if($query->num_rows() == 1 ){
                    return $query->result_array();
                } else{
                    return false;
                }
		}

		function comment($data){
            return $this->db->insert('comments', $data);
        }

        function edit_comment($p_id){

            $var = $this->dashboard_model->update_comment();
            if($var){
                redirect('welocme/posts/'.$p_id);
            } else{
                redirect('welocme/posts/'.$p_id);
            }
        }

        function delete_comment($c_id){
            $del = $this->dashboard_model->d_comment();
            if($del){
                redirect('welocme/posts/'.$c_id);
            } else{
                redirect('welocme/posts/'.$c_id);
            }
        }

        function update_comment(){
            $id = $this->input->post('c_id');
            $comment = $this->input->post('update_comment');

            $data = array('comment' => $comment);
            
            $this->db->where('id', $id);
            $this->db->update('comments', $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else{
                return false;
            }
        }

        function d_comment(){
            $id = $this->input->post('post_id');

            $this->db->where('id', $id);
            $this->db->delete('comments');
            if($this->db->affected_rows()>0){
                return true;
            } else{
                return false;
            }
        }

         function get_cat_name(){
        	$this->db->select('id,cat_name');
        	$query = $this->db->get('category');
        	return $query->result();
        }

        function get_sub_cat_name(){
            $this->db->select('id,sub_cat_name');
            $query = $this->db->get('sub_category');
            return $query->result();
        }

		function fetch_category(){
			$this->db->select('*');
			$this->db->from('category');
			$fetch = $this->db->get();
			return $fetch->result();
		}

		function fetch_sub_category($category){
			$this->db->select('*');
			$this->db->where('cat_name', $category);
			$this->db->from('sub_category');
			$fetch = $this->db->get();
			return $fetch->result();
		}

		function fetch_sub_post($subCategory){
			$this->db->select('*');
			$this->db->where('sub_cat_name', $subCategory);
			$this->db->from('posts');
			$fetch = $this->db->get();
			return $fetch->result();
		}

		function fetch_post($id){
			$this->db->select('*');
			$this->db->where('id', $id);
			$this->db->where('status', 'active');
			$this->db->from('posts');
			$fetch = $this->db->get();
			return $fetch->result();
		}
	}

?>