<?php

	class Admin_model extends CI_Model{

		function user(){
			$this->db->select('*');
			$this->db->from('users');
			$result = $this->db->get();

			if($result->num_rows() > 0){
				return $result->result();
			} else{
				return false;
			}
		}

		function d_user($id){

            $this->db->where('id', $id);
            $this->db->delete('users');
            if($this->db->affected_rows()>0){
                return true;
            } else{
                return false;
            }
        }

        function save_category($data){
        	$this->db->insert('category', $data);
        }

        function get_cat_name(){
        	$this->db->select('id,cat_name');
        	$query = $this->db->get('category');
        	return $query->result();
        }

        function approve($id){
            $data = array('status' => 'active'); 
            $this->db->where('id', $id);
            $this->db->update('posts', $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else{
                return false;
            }
        }

        function pending_delete($id){
            $this->db->where('id', $id);
            $this->db->delete('posts');
            if($this->db->affected_rows() > 0){
                return true;
            } else{
                return false;
            }
        }

        function get_sub_cat_name(){
            $this->db->select('id,sub_cat_name');
            $query = $this->db->get('sub_category');
            return $query->result();
        }

        function save_sub_category($data){
        	$this->db->insert('sub_category', $data);
        }

        function save_post($data){
            $this->db->insert('posts', $data);
        }

        function insert_sticker($data){
            $this->db->insert('sticker', $data);
        }

        function fetch_post(){
            $this->db->select('*');
            
            $this->db->where('status', 'pending');
            $this->db->from('posts');
            $fetch = $this->db->get();
            return $fetch->result();
        }
	}