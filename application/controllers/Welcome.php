<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array('welcome_model'));
	}

	
	function index(){
		$data['sticker'] = $this->welcome_model->fetch_sticker();
		$this->load->view('includes/header');
		$this->load->view('public/home', $data);
		$this->load->view('includes/footer');
	}


	public function explore(){
		$data['category'] = $this->welcome_model->fetch_category();
			$this->load->view('includes/header');
			$this->load->view('public/explore', $data);
			$this->load->view('includes/footer');
	}

	function category($category){
		$data['sub_category'] = $this->welcome_model->fetch_sub_category($category);
		$this->load->view('includes/header');
		$this->load->view('public/category', $data);
		$this->load->view('includes/footer');
	}

	function subCategory($subCategory){

		$data['post'] = $this->welcome_model->fetch_sub_post($subCategory);
		$this->load->view('includes/header');
		$this->load->view('public/sub-category', $data);
		$this->load->view('includes/footer');
	}

	function comment(){
        	$data = array(
        		'c_blog_id' => $this->input->post('c_blog_id'),
        		'c_user_id' => $this->input->post('c_user_id'),
        		'comment' => $this->input->post('comment')
        	);

        	$var = $this->input->post('c_blog_id');

        	$comment['data'] = $this->welcome_model->comment($data);
        	redirect('welcome/posts/'.$var);
        }

	function posts($id){
		$data['post'] = $this->welcome_model->fetch_post($id);
		$data['comment'] = $this->welcome_model->fetch_comment($id);

		$value = $data['comment'];

        	if($value != null){
        		for ($i = 0; $i < count($value); $i++){
                    $uid = $value[$i]->c_user_id;
                    $name = $this->welcome_model->getUserName($uid);
                    $value[$i]->username = $name[0]['username'];
                    $value[$i]->user_image = $name[0]['user_image'];
                }
        	}
		
		$this->load->view('includes/header');
		$this->load->view('public/posts', $data);
		$this->load->view('includes/footer');
	}

	function add_user_post(){
		$data['sub_category'] = $this->welcome_model->get_sub_cat_name();
		$data['category'] = $this->welcome_model->get_cat_name();
		$this->load->view('includes/header');
		$this->load->view('public/add_post_user', $data);
		$this->load->view('includes/footer');
	}

	 function edit_comment($p_id){

            $var = $this->welcome_model->update_comment();
            if($var){
                redirect('welcome/posts/'.$p_id);
            } else{
                redirect('welcome/posts/'.$p_id);
            }
        }

        function delete_comment($c_id){
            $del = $this->welcome_model->d_comment();
            if($del){
                redirect('welcome/posts/'.$c_id);
            } else{
                redirect('welcome/posts/'.$c_id);
            }
        }

	function query(){
		$this->load->view('includes/header');
		$this->load->view('query/check');
		$this->load->view('includes/footer');
	}

	function query_check(){
		//FETCHING CATEGORY TABLE AS ARRAY OF OBJECTS
		/*Array
		(
		    [0] => Array
		        (
		            [c_id] => 1
		            [c_name] => web
		        )

		    [1] => Array
		        (
		            [c_id] => 2
		            [c_name] => design
		        )

		    [2] => Array
		        (
		            [c_id] => 3
		            [c_name] => desktop
		        )

		)*/
		$data = $this->welcome_model->exam_result();
		//END===================


		//FETCHING DATA FROM FORM AS ARRAY
		/*Array
		(
		    [html] => html
		    [js] => js
		)*/
		 $num = $this->input->post();
		 //END====================

		 

		 //FETCHING SKILL.ID FROM SKILL TABLE USING SKILL.NAME DYNAMICALLY BY FOREACH()
		 //AFTER FETCHING $NUM ARRAY CHANGE AS LIKE BELOW COMMENTED ARRAY:
		 /*Array
		(
		    [html] => 2
		    [js] => 3
		)*/
		 $var = $num;
		 foreach($var as $v){

		 	$this->load->database();
			$this->db->select('s_id');
			$this->db->from('skills');
			$this->db->where('s_name', $v);

			$query = $this->db->get();
			$da = $query->result();

			$num[$v]= $da[0]->s_id;
		 };
		 //END==============



		 //GLOBAL VARIABLE
		 $plus = 0;
		 
		 //FETCHING VALUES FROM VALUE BASED ON CATEGORY ID & SKILL ID;
		 for($i=0; $i<count($data); $i++){ //TO FETCH SKILL VALUES TO ASSIGN IN $data['all 3 indexeds']['skills'] ARRAY
			foreach($num as $v){ //GETTING SKILL VALUE OF ALL THE SKILLS('html', 'js') FOR EVERY SINGLE CATEGORY;
			 	 $this->db->select('value');
				 $this->db->from('value');
				 $this->db->where('c_id', $data[$i]['c_id']);
				 $this->db->where('s_id', $v);
				 $query = $this->db->get();

				 $val = $query->result();

				 $plus += $val[0]->value; //ASSIGNING & SUMMATION OF SKILL VALUES;
				
			 } //END FOREACH;

		 	$data[$i]['skills'] = $plus; //ASSIGNING THE WHOLE PERCENTIGE  VALUE TO THE CATEGORIES('web','desktop','design');
		 	$plus = 0; //re initiating plus as 0 because after the foreach executed it means $plus must be starts from 0 to have the absolute value for the next category.
		 };


		 
		 $values['key'] = $data;
		 $this->load->view('includes/header');
		 $this->load->view('query/data', $values);
		 $this->load->view('includes/footer');
		
	}

	function popular_category(){
		$this->load->view('includes/header');
		$this->load->view('public/popular-category');
		$this->load->view('includes/footer');
	}

	function about_us(){
		$this->load->view('includes/header');
		$this->load->view('public/about');
		$this->load->view('includes/footer');
	}

}
