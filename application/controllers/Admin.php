<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('admin_model'));
	}

	public function users()
	{
		$data['user'] = $this->admin_model->user();
		$this->load->view('admin/admin_head');
		$this->load->view('admin/users', $data);
		$this->load->view('admin/admin_foot');
		
	}

	public function delete($id){

		$data = $this->admin_model->d_user($id);

		if($data){
			redirect('admin/users');
		} else{
			redirect('admin/users');
		}
	}

	public function category(){
		$this->load->view('admin/admin_head');
		$this->load->view('auth/category');
		$this->load->view('admin/admin_foot');
	}

	public function save_category(){
		$data = array(

				'cat_name' => $this->input->post('cat_name'),
				'cat_heading' => $this->input->post('cat_heading'),
				'cat_description' => $this->input->post('cat_description'),
				'video_url' => $this->do_upload('video_url')
				
			);
		if($this->admin_model->save_category($data)){
			redirect('admin/sub_category');
		} else{
			redirect('admin/category');
		}
	}

	private function do_upload($value){
		$type = explode('.', $_FILES[$value]["name"]);
		$type = $type[count($type)-1];
		$url = "./assets/uploads/video/".uniqid(rand()).'.'.$type;
		if(in_array($type, array("jpg","jpeg","gif","png","mp4")))
			if(is_uploaded_file($_FILES[$value]["tmp_name"]))
				if(move_uploaded_file($_FILES[$value]["tmp_name"], $url))
					return $url;
		return "";
	}

	public function sub_category(){
		$data['name'] = $this->admin_model->get_cat_name();
		$this->load->view('admin/admin_head');
		$this->load->view('auth/sub-category', $data);
		$this->load->view('admin/admin_foot');
	}

	public function save_sub_category(){
		$data = array(

				'sub_cat_name' => $this->input->post('sub_cat_name'),
				'sub_cat_heading' => $this->input->post('sub_cat_heading'),
				'sub_cat_description' => $this->input->post('sub_cat_description'),
				'sub_cat_video_url' => $this->do_upload('sub_cat_video_url'),
				'cat_name' => $this->input->post("cat_name")
				
			);

		if($this->admin_model->save_sub_category($data)){
			redirect('admin/post');
		} else{
			redirect('admin/sub_category');
		}
	}

	public function post(){
		$data['sub_category'] = $this->admin_model->get_sub_cat_name();
		$data['category'] = $this->admin_model->get_cat_name();
		$this->load->view('admin/admin_head');
		$this->load->view('auth/posts', $data);
		$this->load->view('admin/admin_foot');
	}

	public function pending_post(){
		$data['key'] = $this->admin_model->fetch_post();
		$this->load->view('admin/admin_head');
		$this->load->view('auth/pending_post', $data);
		$this->load->view('admin/admin_foot');
	}


	public function save_post(){
		$data = array(
			'cat_name' => $this->input->post('cat_name'),
			'sub_cat_name' => $this->input->post('sub_cat_name'),
			
			'post_title' => $this->input->post('post_title'),
			'post_heading' => $this->input->post('post_heading'),
			'post_description' => $this->input->post('post_description'),
			'video_url' => $this->do_upload('video_url'),
			'requirments' => $this->input->post('requirments'),
			'institution' => $this->input->post('institution'),
			'demand' => $this->input->post('demand'),
			'skills' => $this->input->post('skills'),
			'conclusion' => $this->input->post('conclusion'),
			'status' => $this->input->post('status')
		);

		if($this->admin_model->save_post($data)){
			redirect('admin/post');
		} else{
			redirect('admin/post');
		}
	}


	public function save_user_post(){
		$data = array(
			'cat_name' => $this->input->post('cat_name'),
			'sub_cat_name' => $this->input->post('sub_cat_name'),
			
			'post_title' => $this->input->post('post_title'),
			'post_heading' => $this->input->post('post_heading'),
			'post_description' => $this->input->post('post_description'),
			'video_url' => $this->do_upload('video_url'),
			'requirments' => $this->input->post('requirments'),
			'institution' => $this->input->post('institution'),
			'demand' => $this->input->post('demand'),
			'skills' => $this->input->post('skills'),
			'conclusion' => $this->input->post('conclusion'),
			'status' => $this->input->post('status')
		);

		if($this->admin_model->save_post($data)){
			redirect('welcome/add_user_post');
		} else{
			redirect('welcome/add_user_post');
		}
	}
	public function approve($id){
		$data = $this->admin_model->approve($id);
		if($data){
			redirect('admin/pending_post');
		} else{
			redirect('admin/pending_post');
		}
	}

	public function pending_delete($id){
		$data = $this->admin_model->pending_delete($id);
		if($data){
			redirect('admin/pending_post');
		} else{
			redirect('admin/pending_post');
		}
	}

	public function add_news(){
		$this->load->view('admin/admin_head');
		$this->load->view('auth/sticker');
		$this->load->view('admin/admin_foot');
	}

	public function save_sticker(){
		$data = array(
			'title' => $this->input->post('title'),
			'url' => $this->input->post('url')
		);
		$value = $this->admin_model->insert_sticker($data);
		if($value){
			redirect('admin/add_news');
		} else{
			redirect('admin/add_news');
		}
	}

	

}
?>
